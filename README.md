# Test cases for automation tests


This is a set of test cases created for automated testing contained in the repository at https://gitlab.com/first7333950/automation-tests

The repository with automated tests is included in the same GitLab profile

Test cases were created based on the TestRail platform. They were exported to pdf and placed in GitLab in order to place both the test cases and the automated test repository in one place. Additionally, people who do not have access to my TestRail account would not be able to view test cases on the TestRail platform.

